# Copyright 2020 ActZero, Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#  http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import os
import uuid
from functools import wraps

from prometheus_client import CollectorRegistry, Counter, push_to_gateway, Summary

# Namespace collection for push gateway
registry = CollectorRegistry()


def create_metric(metric, name, description, labels):
    """
    Create a Prometheus metric instance.
    :param metric: subclass of MetricWrapperBase such as Counter, Gauge, Summary, etc
    :param name: name of the metric
    :param description: metric description
    :param labels: dictionary of labels to use for metric where key is a label name and value is a label value
    :return: metric instance
    """
    label_names = labels.keys()
    return metric(name, description, label_names, registry=registry).labels(**labels)


def push_metrics(func):
    """
    Used to wrap the aws lambda handler function to ensure metrics are pushed
    to the push gateway.
    """

    # Ensure all metrics from the same aws lambda runtime context use the same
    # job id to avoid duplicate metrics
    job_id = uuid.uuid4().hex

    def handler_wrapper(event, context):
        try:
            func(event, context)
        finally:
            push_to_gateway(os.environ.get('METRIC_PUSH_GATEWAY'), job=job_id, registry=registry)

    return handler_wrapper


def handler_invocation_metrics(prefix, labels):
    """
    Decorator for AWS lambda handlers for invocation statistics
    :param prefix: prefix for metric name
    :param labels: labels to apply for all the invocation metrics
    """
    invocations = create_metric(Counter,
                                f'{prefix}_invocations',
                                'The number of times a function has been invoked',
                                labels)

    errors = create_metric(Counter,
                           f'{prefix}_errors',
                           'The number of times a function has errored out',
                           labels)

    successes = create_metric(Counter,
                              f'{prefix}_sucesses',
                              'The number of times a function has succeeded',
                              labels)

    time = create_metric(Summary,
                         f'{prefix}_execution_time',
                         'How long a function takes to execute',
                         labels)

    def handler_decorator(func):
        @time.time()
        @errors.count_exceptions()
        @wraps(func)
        def handler_wrapper(event, context):
            invocations.inc()
            func(event, context)
            successes.inc()

        return handler_wrapper

    return handler_decorator

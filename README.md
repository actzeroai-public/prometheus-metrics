# Prometheus Metrics

A library to simplify integration of Prometheus metrics reporting with your project.

##Installation
```shell script
pip install git+https://gitlab.com/actzeroai-public/prometheus-metrics.git#egg=prometheus-metrics
```

To use as a dependency in another project, add the following line to `requirements.txt`
```
git+https://gitlab.com/actzeroai-public/prometheus-metrics.git#egg=prometheus-metrics
```  

##Usage
```python
from pmetrics import push_metrics, handler_invocation_metrics

@push_metrics
@handler_invocation_metrics("project_prefix_name", {"env": "prod"})
def lambda_handler(event, context):
    pass
```

##Contribution 
All contributions are accepted via merge requests.

##License
Apache 2.0
